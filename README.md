# Teste Técnico Cognitivo

Teste Técnico para vaga na Cognitivo

## Perguntas:
    
    a)Como foi a definição da sua estratégia de modelagem?
    Antes de pensar em uma técnica de modelagem, inicialmente analisei os dados de forma a obter algum indicío para a escolha da técnica.
    Em seguida, realizei as transformações dos dados e apliquei uma série de técnicas para no final, escolher o XGBoostRegressor.
    
    b)Como foi definida a função de custo utilizada?
    Definindo a técnica a ser utilizada, o próximo passo foi escolher a função a ser minimizada.
    A escolha foi a função erro quadrático(squared error). É uma função clássica de problemas de regressão que podemos depois olhar para a esperança da mesma. Ou seja, a funçaõ de erro quadrático médio.
    
    c) Qual foi o critério utilizado na seleção do modelo final?
    O critério utilizado para escolher entre os modelos testados foi o Coeficiente de determinação (R^2) e o valor do MSE(Erro quadrático Médio).
    
    d)Qual foi o critério utilizado para validação do modelo?Por que escolheu utilizar este método?
    O critério utilizado foi a validação cruzada. Essa forma de validação foi escolhida para utilizarmos melhor nosso dataset, além de permitir mais informações sobre o desempenho do modelo.
    
    e) Quais evidências você possui de que seu modelo é suficientemente bom?
    Após a análise dos dados, obtemos um modelo que consegue dar uma ideia de como o preço se comporta. Contudo, uma análise de fatores que não estão no dataset como dia da publicação, indicações de amigos, poderiam enriquecer ainda mais o modelo.
    